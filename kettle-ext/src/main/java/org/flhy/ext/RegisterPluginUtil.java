package org.flhy.ext;

import com.google.common.collect.Lists;
import org.pentaho.di.core.database.DatabaseInterface;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.plugins.*;
import org.pentaho.di.trans.step.StepMetaInterface;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author SangXiebin
 * @date 2018/12/28 9:24
 * 注册插件的工具类
 */
public class RegisterPluginUtil {

    private static List<String> empty = Lists.newArrayList();

    /**
     * 封装注册插件的工具方法
     *
     * @param pluginId    插件ID
     * @param category    插件目录
     * @param name        插件名称
     * @param description 插件描述
     * @param imageFile   插件图片
     * @param pluginClass 插件class
     * @throws KettleException 抛出注册插件异常
     */
    public static void registerPlugin(String[] pluginId, String category, String name, String description,
                                      String imageFile, Class pluginClass) throws KettleException {

        Map<Class<?>, String> pluginClassMap = new HashMap<>();
        pluginClassMap.put(StepMetaInterface.class, pluginClass.getName());
        PluginInterface pluginInterface = new Plugin(
                pluginId,
                StepPluginType.class,
                StepMetaInterface.class,
                category,
                name,
                description,
                imageFile,
                false,
                false,
                pluginClassMap,
                empty,
                null,
                null);
        PluginRegistry.getInstance().registerPlugin(StepPluginType.class, pluginInterface);

    }
    public static void registerDatabasePlugin(String[] pluginId,  String name,
                                      Class pluginClass) throws KettleException {
        Map<Class<?>, String> pluginClassMap = new HashMap<>();
        pluginClassMap.put( DatabaseInterface.class, pluginClass.getName());
        PluginInterface pluginInterface = new Plugin(
                pluginId,
                DatabasePluginType.class,
                DatabaseInterface.class,
                null,
                name,
                null,
                "./\\null",
                false,
                false,
                pluginClassMap,
                empty,
                null,
                null);
        PluginRegistry.getInstance().registerPlugin(DatabasePluginType.class, pluginInterface);
    }
}
