package org.flhy.ext;

import org.apache.commons.dbcp.BasicDataSource;
import org.flhy.ext.core.PropsUI;
import org.pentaho.di.core.DBCache;
import org.pentaho.di.core.RowMetaAndData;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.logging.*;
import org.pentaho.di.core.row.RowMeta;
import org.pentaho.di.job.JobExecutionConfiguration;
import org.pentaho.di.repository.LongObjectId;
import org.pentaho.di.repository.ObjectId;
import org.pentaho.di.repository.Repository;
import org.pentaho.di.repository.kdr.KettleDatabaseRepository;
import org.pentaho.di.repository.kdr.KettleDatabaseRepositoryMeta;
import org.pentaho.di.trans.TransExecutionConfiguration;
import org.pentaho.metastore.stores.delegate.DelegatingMetaStore;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.ArrayList;
@Component
public class App implements ApplicationContextAware {

	private static App app;
	public static KettleDatabaseRepositoryMeta meta;

	private LogChannelInterface log;
	private TransExecutionConfiguration transExecutionConfiguration;
	private TransExecutionConfiguration transPreviewExecutionConfiguration;
	private TransExecutionConfiguration transDebugExecutionConfiguration;
	private JobExecutionConfiguration jobExecutionConfiguration;
	public PropsUI props;

	private App() {
		props = PropsUI.getInstance();
		log = new LogChannel( PropsUI.getAppName());
		loadSettings();

		transExecutionConfiguration = new TransExecutionConfiguration();
		transExecutionConfiguration.setGatheringMetrics( true );
		transPreviewExecutionConfiguration = new TransExecutionConfiguration();
		transPreviewExecutionConfiguration.setGatheringMetrics( true );
		transDebugExecutionConfiguration = new TransExecutionConfiguration();
		transDebugExecutionConfiguration.setGatheringMetrics( true );

		jobExecutionConfiguration = new JobExecutionConfiguration();

		variables = new RowMetaAndData( new RowMeta() );
	}

	public void loadSettings() {
		LogLevel logLevel = LogLevel.getLogLevelForCode(props.getLogLevel());
		DefaultLogLevel.setLogLevel(logLevel);
		log.setLogLevel(logLevel);
		KettleLogStore.getAppender().setMaxNrLines(props.getMaxNrLinesInLog());

		// transMeta.setMaxUndo(props.getMaxUndo());
		DBCache.getInstance().setActive(props.useDBCache());
	}

	public static App getInstance() {
		if (app == null) {
			app = new App();
		}
		return app;
	}

	private Repository repository;

	public Repository getRepository() {
		initRepository();
		return repository;
	}

	private Repository defaultRepository;

//	public void initDefault(Repository defaultRepo) {
//		if(this.defaultRepository == null)
//			this.defaultRepository = defaultRepo;
//		this.repository = defaultRepo;
//	}

	public Repository getDefaultRepository() {
		return this.defaultRepository;
	}

	public void selectRepository(Repository repo) {
		if(repository != null) {
			repository.disconnect();
		}
		repository = repo;
	}

	private DelegatingMetaStore metaStore;

	public DelegatingMetaStore getMetaStore() {
		return metaStore;
	}

	public LogChannelInterface getLog() {
		return log;
	}

	private RowMetaAndData variables = null;
	private ArrayList<String> arguments = new ArrayList<String>();

	public String[] getArguments() {
		return arguments.toArray(new String[arguments.size()]);
	}

	public JobExecutionConfiguration getJobExecutionConfiguration() {
		return jobExecutionConfiguration;
	}

	public TransExecutionConfiguration getTransDebugExecutionConfiguration() {
		return transDebugExecutionConfiguration;
	}

	public TransExecutionConfiguration getTransPreviewExecutionConfiguration() {
		return transPreviewExecutionConfiguration;
	}

	public TransExecutionConfiguration getTransExecutionConfiguration() {
		return transExecutionConfiguration;
	}

	public RowMetaAndData getVariables() {
		return variables;
	}

	public void initRepository(){
		if(repository!=null){return;}
		KettleDatabaseRepository repository = new KettleDatabaseRepository();
		try {
			DatabaseMeta dbMeta = new DatabaseMeta();
			String hostname = "192.168.1.240";
			String port = "3306";
			String dbName = "kettle";
			dbMeta.setName("192.168.1.240_kettle");
			dbMeta.setDBName(dbName);
			dbMeta.setDatabaseType("MYSQL");
			dbMeta.setAccessType(0);
			dbMeta.setHostname(hostname);
			dbMeta.setServername(hostname);
			dbMeta.setDBPort(port);
			dbMeta.setUsername("root");
			dbMeta.setPassword("root");
			ObjectId objectId = new LongObjectId(100);
			dbMeta.setObjectId(objectId);
			dbMeta.setShared(true);
			dbMeta.addExtraOption(dbMeta.getPluginId(), "characterEncoding", "utf8");
			dbMeta.addExtraOption(dbMeta.getPluginId(), "useUnicode", "true");
			dbMeta.addExtraOption(dbMeta.getPluginId(), "autoReconnect", "true");
			meta = new KettleDatabaseRepositoryMeta();
			meta.setName("192.168.1.240_kettle");
			meta.setId("KettleDatabaseRepository");
			meta.setConnection(dbMeta);
			meta.setDescription("192.168.1.240_kettle");
			repository.init(meta);
			repository.connect("admin", "admin");
			this.repository = repository;
			System.out.println("资源库连接成功");
		} catch (Exception e) {
			System.out.println("资源库连接失败");
		}
	}
	@Override
	public void  setApplicationContext(ApplicationContext context) throws BeansException {
		DataSource dataSource = (DataSource)context.getBean("dataSource");
		if(dataSource!=null){
			System.out.println("连接数据库成功");
			System.out.println("数据库url"+dataSource.toString());
		}

	}


}
