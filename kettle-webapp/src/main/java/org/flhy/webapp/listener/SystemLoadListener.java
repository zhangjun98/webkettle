package org.flhy.webapp.listener;

import org.flhy.ext.RegisterPluginUtil;
import org.flhy.ext.core.PropsUI;
import org.pentaho.big.data.kettle.plugins.hdfs.trans.HadoopFileInputMeta;
import org.pentaho.big.data.kettle.plugins.hdfs.trans.HadoopFileOutputMeta;
import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.Props;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.logging.KettleLogStore;
import org.pentaho.di.job.entries.checkfilelocked.JobEntryCheckFilesLocked;
import org.pentaho.di.repository.filerep.KettleFileRepositoryMeta;
import org.pentaho.di.trans.steps.checksum.CheckSumMeta;
import org.pentaho.di.trans.steps.jsoninput.JsonInputMeta;
import org.pentaho.di.trans.steps.jsonoutput.JsonOutputMeta;
import org.pentaho.di.trans.steps.validator.Validator;
import org.sxdata.jingwei.util.TaskUtil.CarteTaskManager;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.File;

public class SystemLoadListener implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent context) {

    }

    @Override
    public void contextInitialized(ServletContextEvent context) {
//		System.setProperty(Const.KETTLE_CORE_STEPS_FILE, "org/flhy/ext/kettle-steps-file.xml");
        try {
            System.out.println("开启carte服务线程...");
            CarteTaskManager.startThread(1);//启动1个线程处理执行carte服务
            // 日志缓冲不超过5000行，缓冲时间不超过720秒
            KettleLogStore.init(5000, 720);
            KettleEnvironment.init();
//			Props.init( Props.TYPE_PROPERTIES_KITCHEN );
            PropsUI.init("KettleWebConsole", Props.TYPE_PROPERTIES_KITCHEN);
//			String path = context.getServletContext().getRealPath("/reposity/");
            File path = new File("samples/repository");
            KettleFileRepositoryMeta meta = new KettleFileRepositoryMeta();
            meta.setBaseDirectory(path.getAbsolutePath());
            meta.setDescription("default");
            meta.setName("default");
            meta.setReadOnly(false);
            meta.setHidingHiddenFiles(true);

            //Thread.sleep(1 * 1000L);
            //开启作业定时
            //CarteTaskManager.startJobTimeTask();
            addPlugins();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 注册自定义插件
     *
     * @throws KettleException
     */
    private static void addPlugins() throws KettleException {
        // 注册Json Input插件
        RegisterPluginUtil.registerPlugin(new String[]{"JsonInput"}, "输入", "json 输入",
                "json 输入", "jsonInput.svg", JsonInputMeta.class);

        // 注册Json Output插件
        RegisterPluginUtil.registerPlugin(new String[]{"JsonOutput"}, "输出", "json 输出",
                "json 输出", "jsonOutput.svg", JsonOutputMeta.class);

        RegisterPluginUtil.registerPlugin(new String[]{"HadoopFileInput"},"big data", "hadoop文件输出",
                "hadoop文件输出", "HDI.svg", HadoopFileInputMeta.class);

        // 注册 hadoop 文件输出插件
        RegisterPluginUtil.registerPlugin(new String[]{"HadoopFileOutput"},"大数据", "hadoop文件输出",
                "hadoop文件输出", "HDO.svg", HadoopFileOutputMeta.class);

        RegisterPluginUtil.registerPlugin(new String[]{"CheckSumMeta"},"big data", "检查sum",
                "检查sum", "HDI.svg", CheckSumMeta.class);

        RegisterPluginUtil.registerPlugin(new String[]{"Validator"},"big data", "检验",
                "检验", "HDI.svg", Validator.class);

// org.pentaho.di.trans.steps.checksum.CheckSumMeta
// org.pentaho.di.trans.steps.mongodbinput.MongoDbInputMeta
        //org.pentaho.di.job.entries.checkfilelocked.JobEntryCheckFilesLocked


    }
}
